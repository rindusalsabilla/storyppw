from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story8UnitTest(TestCase):

    def test_story9_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story9_url_is_exist_book(self):
        response = Client().get('/searchbook/')
        self.assertEqual(response.status_code,404)

    def test_story9_url_is_exist_logout(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code,404)

    def test_lab_9_json_file_is_exist(self):
        response = Client().get('/searchJson?cari=')
        self.assertEqual(response.status_code, 200)

    def test_lab_11_using_increasefav(self):
        found = resolve('/increasefav')
        self.assertEqual(found.func, increasefav)

    def test_lab_11_using_decreasefav(self):
        found = resolve('/decreasefav')
        self.assertEqual(found.func, decreasefav)

    def test_story9_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_template_home_story9(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'book.html')

    def test_landing_page_(self):
         response = Client().get('/')
         html_response = response.content.decode('utf-8')
         self.assertIn('List of Books',html_response)

    def test_landing_page2_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Favorites',html_response)

    def test_landing_page3_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Login with Google',html_response)
